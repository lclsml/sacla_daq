import numpy as np
import xrt.backends.raycing.materials as mat
import math

__all__ = ['SatelliteGeometry']


class CrystalGe(mat.CrystalDiamond):
    def __init__(self, hkl, calcBorrmann=True):
        self.a = 5.65735
        self.d = np.sqrt(self.a ** 2 / (hkl[0] ** 2 + hkl[1] ** 2 + hkl[2] ** 2))
        super().__init__(hkl, d=self.d, elements='Ge', factDW=1.0, nuPoisson=0.28, calcBorrmann=calcBorrmann)


class CrystalSi(mat.CrystalDiamond):
    def __init__(self, hkl, calcBorrmann=True):
        self.a = 5.4307717932001225
        self.d = np.sqrt(self.a ** 2 / (hkl[0] ** 2 + hkl[1] ** 2 + hkl[2] ** 2))
        super().__init__(hkl, d=self.d, elements='Si', factDW=1.0, nuPoisson=0.27, calcBorrmann=calcBorrmann)


class SatelliteGeometry(object):
    def __init__(self, do, f, EKa1=6405, npixels=1024, pixel_size=0.05):
        self.xtal = CrystalGe([4, 4, 0])
        theta0 = math.pi / 2 - self.xtal.get_Bragg_angle(EKa1)
        self.do = do
        self.f = f
        self.theta0 = theta0
        self.delta_focus = 0
        self.delta_do = 0
        self.pixel_size = pixel_size
        self.npixels = npixels
        self.eup = np.linspace(EKa1, EKa1 + 100, 50)
        delta_e = self.eup[1] - self.eup[0]
        self.edown = self.eup[0] - delta_e * np.arange(50, 0, -1)
        self.energies = np.concatenate([self.edown, self.eup])[::-1]

    def di(self):
        return 1 / (1 / self.f - 1 / (self.do + self.delta_do))

    def dc(self):
        return (self.do + self.delta_do) * math.cos(self.theta0)

    def db(self):
        return (self.do + self.delta_do) * math.sin(self.theta0)

    def ddc(self):
        return (self.di() + self.delta_focus) * math.cos(self.theta0)

    def l0p(self):
        return (self.ddc() + self.dc()) * math.tan(self.theta0)

    def theta_e(self, e):
        return math.pi / 2 - self.xtal.get_Bragg_angle(e)

    def le(self, e):
        return self.dc() * math.tan(self.theta_e(e))

    def ddet(self):
        return self.ddc() - self.dc()

    def lep(self, e):
        return math.tan(self.theta_e(e)) * (self.ddc() + self.dc())

    def delta_lep(self, e):
        return self.l0p() - self.lep(e)

    def delta_lep_detuned(self, e, theta_det):
        return self.delta_lep(e) * (
            math.sin(math.pi / 2 - self.theta_e(e)) / math.sin(math.pi / 2 + self.theta_e(e) - theta_det * math.pi / 180))

    def __call__(self, Ka1_pixel, theta_det=None, delta_focus=None, do=None):
        if do is None:
            self.delta_do = 0
        else:
            self.delta_do = do

        if theta_det is None:
            theta_det = 0

        if delta_focus is None:
            self.delta_focus = 0
        else:
            self.delta_focus = delta_focus

        assert Ka1_pixel >= 0 and Ka1_pixel <= (self.npixels - 1)
        calib_space = [self.delta_lep_detuned(e, theta_det) for e in self.energies]
        det_space = np.arange(self.npixels) * self.pixel_size - Ka1_pixel * self.pixel_size
        det_energies = np.interp(det_space, calib_space, self.energies)
        return det_energies
