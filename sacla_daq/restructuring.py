import h5py
import numpy as np

__all__ = ['get_images', 'write_data']

def get_run_list(fid):
    return list(fid['file_info/run_number_list'])


def get_tag_list(fid, run):
    return list(fid['run_{:d}/event_info/tag_number_list'.format(run)])


def get_2d_detector_list(fid, run):
    return list(fid['run_{:d}/run_info/detector_config/detector_2d_list'.format(run)])

def get_images(fid):
    runs = get_run_list(fid)
    data = {}
    for run in runs:
        tags = get_tag_list(fid, run)
        dets = get_2d_detector_list(fid, run)
        images = {}
        for det in dets:
            shape = np.array(fid['run_{:d}/detector_2d_{:d}/tag_{:d}/detector_data'.format(run, det, tags[0])]).shape
            dtype = np.array(fid['run_{:d}/detector_2d_{:d}/tag_{:d}/detector_data'.format(run, det, tags[0])]).dtype
            images['{:d}'.format(det)] = (np.empty([len(tags)] + list(shape), dtype=dtype))
            for k, tag in enumerate(tags):
                images['{:d}'.format(det)][k, :, :] = np.array(
                    fid['run_{:d}/detector_2d_{:d}/tag_{:d}/detector_data'.format(run, det, tag)])
        data['{:d}'.format(run)] = images
    return data


def write_data(fname, dict_data):
    """

    :param fname: file name to write out to
    :param dict_data: a dictionary containing data.
    :return: nothing

    dict_data is expected to be of the form:
        <level 1> string id:
            <level 2> string id: data
            <level 2> string id: data

    This function will over-write any data found in the file
    that has a matching <level 1>/<level 2> group name
    """
    with h5py.File(fname, 'a') as fid:
        for key, val in dict_data.items():
            path = descend(fid, '', key)
            for key2, val2 in val.items():
                dname = path + '/{:s}'.format(key2)
                fid.require_dataset(dname, shape=val2.shape, dtype=val2.dtype)
                fid[dname][...] = val2


def descend(fid, path, key):
    path = path + '{:s}'.format(key)
    fid.require_group(path)
    return path
