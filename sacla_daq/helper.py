#!/bin/env python
import sys
import subprocess

__all__ = ['get_start_tag', 'get_end_tag', 'make_dark_tags', 'make_light_tags']

def get_start_tag(run):
    """
    returns the start tag number of a run if it exists, else False
    """
    try:
        result = subprocess.run(f"ShowRunInfo -b 3 -r {run} | grep -o [0-9]*-[0-9]* | grep -o ^[0-9]*",
                                shell=True, check=True, stdout=subprocess.PIPE)
        result = result.stdout.decode('utf8')
        return int(result)
    except subprocess.CalledProcessError as e:
        return False

def get_end_tag(run):
    """
    returns the start tag number of a run if it exists, else False
    """
    try:
        result = subprocess.run(f"ShowRunInfo -b 3 -r {run} | grep -o [0-9]*-[0-9]* | grep -o [0-9]*$",
                                shell=True, check=True, stdout=subprocess.PIPE)
        result = result.stdout.decode('utf8')
        return int(result)
    except subprocess.CalledProcessError as e:
        return False

def make_dark_tags(run, det_string, out_loc, N = 150):
    start_tag = get_start_tag(run)
    end_tag = get_end_tag(run)
    if start_tag == False or end_tag == False:
        raise Exception('Run does not exist')
    elif end_tag - start_tag < 2*N:
        raise Exception('Run is not long enough')
    else:
        end_tag = start_tag + 2*N
        cmd = f"MakeTagList -b 3 -r {run} -det {det_string} -starttag {start_tag} -endtag {end_tag} -out {out_loc}"
        #print(cmd)
        try:
            subprocess.run(cmd,shell=True, check=True, stdout=subprocess.PIPE)
        except subprocess.CalledProcessError() as e:
            print("Somethng went wrong in the MakeTagList call")
            raise e

def make_light_tags(run, det_string, out_loc, N = 150):
    start_tag = get_start_tag(run)
    end_tag = get_end_tag(run)
    if start_tag == False or end_tag == False:
        raise Exception('Run does not exist')
    elif end_tag - start_tag < 2*N:
        raise Exception('Run is not long enough')
    else:
        start_tag = start_tag + 2*N + 2
        cmd = f"MakeTagList -b 3 -r {run} -det {det_string} -starttag {start_tag} -endtag {end_tag} -out {out_loc}"
        #print(cmd)
        try:
            subprocess.run(cmd, shell=True, check=True, stdout=subprocess.PIPE)
        except subprocess.CalledProcessError() as e:
            print("Somethng went wrong in the MakeTagList call")
            raise e

if __name__ == '__main__':
    with sys.stdin as f:
        run = int(f.readline())
        print(get_start_tag(run))
        print(get_end_tag(run))
