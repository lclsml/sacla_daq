from setuptools import setup

setup(
    name='sacla_daq',
    version='v0.1',
    packages=['sacla_daq', 'snakemake'],
    url='https://bitbucket.org/lclsml/sacla_daq',
    license='',
    author='Franklin Fuller',
    author_email='fdfuller@slac.stanford.edu',
    description='Things to get data at SACLA'
)
