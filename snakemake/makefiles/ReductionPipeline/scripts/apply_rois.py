import _utils
import dask
import dask.array as da
import h5py

def apply(run, input, output, threads, registry):
    """apply ROIs to repacked datasets from registry"""
    # load registry and lookup roi
    registry = _utils.load_registry(registry)
    cfg = _utils.lookup_rois(run, registry)
    
    dask.config.set(num_workers=threads)
    
    dets = {}
    with h5py.File(input, 'r', libver='latest') as dinput, h5py.File(output, 'w', libver='latest') as doutput:
        # load up arrays
        sase = da.from_array(dinput[f'{run}/2'], chunks=(100, None, None)) 
        fluor = da.from_array(dinput[f'{run}/3'], chunks=(100, None, None)) 
        trans = da.from_array(dinput[f'{run}/1'], chunks=(100, None, None)) 
        
        # apply rois
        sase = sase[:, slice(*cfg.sase_rr_window), slice(*cfg.sase_cc_window)]
        fluor = fluor[:, slice(*cfg.fluor_rr_window), slice(*cfg.fluor_cc_window)]
        trans = trans[:, slice(*cfg.trans_rr_window), slice(*cfg.trans_cc_window)]
        
        doutput.create_dataset(f'{run}/2', data=sase.compute())
        doutput.create_dataset(f'{run}/3', data=fluor.compute())
        doutput.create_dataset(f'{run}/1', data=trans.compute())

if __name__ == '__main__':
    run = int(snakemake.wildcards.run)
    input = snakemake.input[0]
    output = snakemake.output[0]
    threads = snakemake.threads
    registry = snakemake.params.registry

    apply(run, input, output, threads, registry)

