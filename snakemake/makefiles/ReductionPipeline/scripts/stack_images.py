import h5py
import pathlib

inp = snakemake.input[0]
outp = snakemake.output[0]

def process_run(run, src, dst):
    tags = src[f'run_{run}/event_info/tag_number_list'][:]
    dets = src[f'run_{run}/run_info/detector_config/detector_2d_list'][:]
    
    src_fname = str(pathlib.Path(src.filename).absolute())
    
    for det in dets:
        sframe = src[f'run_{run}/detector_2d_{det}/tag_{tags[0]}/detector_data']
        layout = h5py.VirtualLayout(shape=(tags.shape[0],) + sframe.shape, dtype=sframe.dtype)
        for i, tag in enumerate(tags):
            layout[i] = h5py.VirtualSource(src_fname, f'run_{run}/detector_2d_{det}/tag_{tag}/detector_data', shape=sframe.shape)
        dst.create_virtual_dataset(f'{run}/{det}', layout)

with h5py.File(inp, 'r', libver='latest') as source:
    runs = source['file_info/run_number_list'][:]
    
    # create virtual sources for stuff
    with h5py.File(outp, 'w') as target:
        for run in runs:
            process_run(run, source, target)
