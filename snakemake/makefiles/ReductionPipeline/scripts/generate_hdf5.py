import subprocess
import _utils
import pathlib
import sys

def make_tags(run, tag_range, detectors, output):
    # make tags based on range
    if tag_range is None:
        # touch output file instead of making one to make snakemake happy
        subprocess.run(f"touch \"{output}\"", shell=True)
        return
        
    # actually make tags
    result = subprocess.run(f"MakeTagList -b 3 -r {run} -det \"{detectors}\" -out \"{output}\" -starttag {tag_range[0]} -endtag {tag_range[1]}", shell=True, check=True)
        
def generate_hdf5(run, ndarks, detectors, dataconvert_config, outputs):
    # get info about the run
    info = _utils.get_run_info(run)
    
    light_taglist, dark_taglist, dark_avg, light_avg, light_hdf5 = outputs
    
    tmp = pathlib.Path(light_hdf5)
    output_dir = str(tmp.parent)
    light_hdf5 = tmp.name

    print(output_dir, light_hdf5)
    print(info.ntrigger, ndarks)

    if info.ntrigger > 2*ndarks: 
        # only perform dark subtraction if nshots is greater than 2*ndarks
        light_range = (info.start_tag + ndarks + 1, info.end_tag)
        dark_range = (info.start_tag,  info.start_tag + ndarks)
    else:
        # run is too short to run dark subtraction, just get it all
        light_range = (info.start_tag, info.end_tag)
        dark_range = None

    # make both tags
    print("Making Tags:")
    make_tags(run, light_range, detectors, light_taglist)
    make_tags(run, dark_range, detectors, dark_taglist)
    
    if dark_range is not None:
        # make dark average
        print("\nMaking dark average:")
        subprocess.run(f"ImgAvg -l \"{dark_taglist}\" -out \"{dark_avg}\"", shell=True, check=True)
        
        print("\nMaking light average:")
        subprocess.run(f"ImgAvg -l \"{light_taglist}\" -out \"{light_avg}-tmp\"", shell=True, check=True)
        subprocess.run(f"ImgLA -x \"{light_avg}-tmp\" -y \"{dark_avg}\" -out \"{light_avg}\"; rm \"{light_avg}-tmp\"", shell=True, check=True)

        print("\nMaking light data:")
        subprocess.run(f"DataConvert4 -l \"{light_taglist}\" -f \"{dataconvert_config}\" -dir \"{output_dir}\" -o \"{light_hdf5}\" -bkg \"{dark_avg}\"", shell=True, check=True)
    else:
        subprocess.run(f"touch \"{dark_avg}\"", shell=True)

        print("\nMaking light average:")
        subprocess.run(f"ImgAvg -l \"{light_taglist}\" -out \"{light_avg}\"", shell=True, check=True)
        #subprocess.run(f"touch \"{light_avg}\"", shell=True)

        print("\nMaking light data:")
        subprocess.run(f"DataConvert4 -l \"{light_taglist}\" -f \"{dataconvert_config}\" -dir \"{output_dir}\" -o  \"{light_hdf5}\"", shell=True, check=True)

    print("Done")

if __name__ == '__main__':
    run = int(snakemake.wildcards.run)
    detectors = snakemake.params.detectors
    dataconvert_config = snakemake.params.dataconvert_config
    ndarks = int(snakemake.params.ndarks)
    output = snakemake.output
    
    generate_hdf5(run, ndarks, detectors, dataconvert_config, output)
