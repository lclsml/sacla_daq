import subprocess
import re
import typing
import json
import attr

@attr.s(auto_attribs=True)
class ROIConfig:
    upto_run: int = -1 # use this configuration upto run `upto_run`
    sase_rr_window: typing.Tuple[int, int] = (None, None) # rows index range
    sase_cc_window: typing.Tuple[int, int] = (None, None) # columns index range
    trans_rr_window: typing.Tuple[int, int] = (None, None)
    trans_cc_window: typing.Tuple[int, int] = (None, None)
    fluor_rr_window: typing.Tuple[int, int] = (None, None)
    fluor_cc_window: typing.Tuple[int, int] = (None, None)

def load_registry(path):
    with open(path, 'r') as f:
        result = json.load(f)
        registry = []
        for c in result:
            registry.append(ROIConfig(**c))
    
    return registry

def save_registry(registry, path):
    with open(path, 'w') as f:
        ser = []
        for c in registry:
            ser.append(attr.asdict(c))
        
        json.dump(ser, f)

def lookup_rois(run, registry):
    assert len(registry) > 0, "must have at least one config"
    registry = sorted(registry, key=lambda item: item.upto_run)
    
    for cfg in registry:
        if run <= cfg.upto_run:
            break
    
    return cfg

@attr.s(auto_attribs=True)
class RunInfo:
    beamline: int = attr.ib()
    run: int = attr.ib()
    status: str = attr.ib()
    comment: str = attr.ib()
    ntrigger: int = attr.ib()
    start_tag: int = attr.ib()
    end_tag: int = attr.ib()

def get_run_info(run):
    #Just look at this fucking monument to a waste of development time and more importantly my time to even read it.
    patterns = [
        r"Status\s+:(?P<status>[\w\(\)\s]+)$",
        r"BeamLine\s+:(?P<beamline>[\d]+)$",
        r"RunNumber:(?P<run>[\d]+)$",
        r"Comment\s+:(?P<comment>[\w\d\s\(\).]+)",
        r"Trigger\s+:(?P<ntrigger>[\d]+)trigger (?P<start_tag>[\d]+)-(?P<end_tag>[\d]+)",
    ]

    job_pattern = re.compile('(' + '|'.join(patterns) + ')', re.M)

    try:
        res = subprocess.run(f"ShowRunInfo -b 3 -r {run}", check=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=True)
        miter = job_pattern.finditer(res.stdout.decode())

        if not miter:
            print("failed")
            sys.exit(-1)
        
        d = {}
        for m in miter:
            for k, v in m.groupdict().items():
                d[k] = v if v is not None else d.get(k, None)

        return RunInfo(
            beamline=d['beamline'],
            run=int(d['run']),
            status=d['status'],
            ntrigger=int(d['ntrigger']),
            start_tag=int(d['start_tag']),
            end_tag=int(d['end_tag']),
            comment=d['comment'],
            )

    except (subprocess.CalledProcessError, IndexError, KeyboardInterrupt) as e:
        raise RuntimeError('could not get run info')
