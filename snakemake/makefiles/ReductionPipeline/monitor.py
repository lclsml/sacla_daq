#!/bin/env python

import time
import subprocess
import click
from concurrent.futures import ThreadPoolExecutor

def check_run_exists(runno):
    try:
        result = subprocess.run(f"ShowRunInfo -b 3 -r {runno}", shell=True, check=True, stdout=subprocess.PIPE)
        status = result.stdout.decode('utf8')
        if 'Stopped (Ready to Read)' in status:
            return True
        
        return False
    except subprocess.CalledProcessError as e:
        # returns error if run number hasn't been used
        return False
    
    return True

def get_run_list(start_run, end_run=None):
    runs = []
    current_run = start_run
    end_run = start_run + 1000 if end_run is None else end_run
    while True:
        if check_run_exists(current_run) and current_run < end_run + 1:
            runs.append(current_run)
        else:
            break

        current_run += 1
    return runs

def submit(run):
    print(f"Submitting job {run} to cluster.")
    
    target_files = [f'flags/{run}.done']
    #target_files = [f'/work/fuller/dataconvert/{run}.h5']
    #target_files = [f'/work/fuller/reduced/{run}_reduced.h5']
    subprocess.run(f"snakemake --profile pbs-pro2 {' '.join(target_files)}", shell=True)

@click.command()
@click.argument('start', type=int)
def monitor(start):
    """run snakemake pipeline for new runs found starting from START"""
    processed = set()
    with ThreadPoolExecutor(max_workers=40) as pool:
        try:
            while True:
                new = set(get_run_list(start))
                delta = new - processed

                if len(delta) > 0:
                    print(f'Found {len(delta)} new runs')
                
                for run in delta:
                    pool.submit(submit, run)

                processed = processed.union(delta)
                time.sleep(5.)
        except KeyboardInterrupt as e:
            print("terminating")

if __name__ == '__main__':
    monitor()
