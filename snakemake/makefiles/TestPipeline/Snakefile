import utils

workdir: "/work/fuller"

rule all:
    input:
        expand("repack/{run}_repacked.h5", run=utils.get_run_list(550758, 550760)),
        expand("repack/{run}_avg_repacked.h5", run=utils.get_run_list(550758, 550760))

rule generate_tags:
    wildcard_constraints:
        run="\d+"
    output:
        "taglists/{run}.lst"
    shell:
        "MakeTagList -b 3 -r {wildcards.run} -det \"MPCCD-1B1-M03-003, MPCCD-1N0-M02-001\" -out {output}"

rule generate_hdf5:
    wildcard_constraints:
        run="\d+"
    params:
        dataconvert_prefix = "/work/fuller/dataconvert"
    input:
        "taglists/{run}.lst"
    output:
        "dataconvert/{run}.h5"
    shell:
        "DataConvert4 -l {input} -f /home/fuller/DataConvert/formats/minus_acquiris_and_bld -dir {params.dataconvert_prefix} -o {wildcards.run}.h5"

rule generate_imgavg:
    wildcard_constraints:
        run="\d+"
    input:
        "taglists/{run}.lst"
    output:
        "img_avg/{run}.h5"
    shell:
        "ImgAvg -l {input} -out {output}"

rule repack_dataconvert_hdf5:
    wildcard_constraints:
        run="\d+"
    input:
        "dataconvert/{run}.h5"
    output:
        "repack/{run}_repacked.h5"
    script:
        "/home/fuller/sacla_daq/snakemake/scripts/stack_images.py"

rule repack_imgavg_hdf5:
    wildcard_constraints:
        run="\d+"
    input:
        "img_avg/{run}.h5"
    output:
        "repack/{run}_avg_repacked.h5"
    script:
        "/home/fuller/sacla_daq/snakemake/scripts/stack_images.py"
