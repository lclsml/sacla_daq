#!/bin/bash
echo "Starting monitoring runs. Press [CTRL+C] to stop..."

while true
do
    snakemake --profile pbs-pro
    sleep 60
done
