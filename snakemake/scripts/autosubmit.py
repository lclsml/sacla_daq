import time
import subprocess

template = """snakemake --cluster "qsub" --jobs 100 {}"""

def check_run_exists(runno):
    try:
        result = subprocess.run(f"ShowRunInfo -b 3 -r {runno}", shell=True, check=True, stdout=subprocess.PIPE)
        status = result.stdout.decode('utf8')
        if 'Stopped (Ready to Read)' in status:
            return True
        
        return False
    except subprocess.CalledProcessError as e:
        # returns error if run number hasn't been used
        return False
    
    return True

def get_run_list(start_run):
    runs = []
    current_run = start_run
    while True:
        if check_run_exists(current_run):
            runs.append(current_run)
        else:
            break
        current_run += 1
    return runs

def submit_job(runno):
    pass

#@click.command()
def monitor():
    pass

if __name__ == '__main__':
    print(get_run_list(745032))
