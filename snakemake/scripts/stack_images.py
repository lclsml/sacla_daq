from sacla_daq import *
import h5py

with h5py.File(snakemake.input[0]) as fid:
    data = get_images(fid)
write_data(snakemake.output[0], data)
